# Full-Text Search

## What is Full-Text Search?

Full-Text Search is an advanced technique, to search text data in a Database or document with optimization. Full-Text Search technique works fast to retrieve data as compare to other searching techniques. It uses an inverted index data structure to convert the content of the database to a full-text index and search data using an index. In the real scenario, It’s something like searching the meaning of a word in a dictionary by scanning all word page wise to rather search the index of the word at the back of the dictionary. Today’s all the big companies like Google, having a large amount of data on their databases using Full-text search techniques for searching content on their site.


# How to implement Full-Text Search?

To implement Full-text search, there are so many full-text search engines available. The most popular ones are Lucene Apache, Apache Solr, and Elastic Search.

## Apache Lucene

Apache Lucene is an open source search engine software and library which is written on java. It uses full-text search techniques for searching text data in a database. Elasticsearch and Apache Solr, the two most popular search engines were built on top of Apache Lucene.

## Apache Solr

Apache Solr is an open source server, provides all the lucence’s search capability through HTTP requests. It’s a NoSQL database. It provides powerful features such as Distributed full-text search, real-time indexing, high availability, NoSQL Database, and integration with big tools like Hadoop. It’s more suitable for an enterprise application that uses a massive amount of static data.

## Elasticsearch

Elasticsearch is also an open-source search engine built on top of Apache Lucene. It uses Lucene indexing and search functionalities using RESTful APIs. It distributed data on multiple servers using the concept of index and shard. Elastic search is using a document-oriented approach when manipulating data that is stored in JSON format. Elasticsearch features include full-text distributed search, high availability, Geo search, and horizontal scaling. Elasticsearch is more focused on scaling, processing time-series data to obtain meaningful insights and patterns.

# Apache Solr  vs. Elasticsearch 
* To install Elasticsearch and Solr both require Java. As compare to Solr Elasticsearch is easy to install and configure.
  
* For Searching, both Solr and Elasticsearch support real-time search and extend Lucene searching capabilities.


* Solr uses request handler to ingest data from XML files,CSV files ,PDFs etc. Otherside, Elasticsearch is completely JSON-based. it supports data ingestion from multiple sources using Elastic Stack and Logstash.
  
* On indexing, Both are schemaless, which helps them to index unstructured data easily. Both, support synonym-based indexing, stemming, and various tokenization process.

* On scalability, Both supports sharding. But Elasticsearch design has horizontal scaling, that why it has better support for scaling and cluster management. In the case of shard or node failure, elastic search cluster rebalance itself while Solr is hard to manage.

* On Documentation, [Elasticsearch](https://www.elastic.co/guide/index.html?ultron=[EL]-[B]-[Stack]-[Trials]-[EMEA]-[UK]-Exact&gambit=Elasticsearch-Documentation&blade=adwords-s&thor=elasticsearch%20documentation&gclid=EAIaIQobChMIyoaRu6nZ6gIVkqkYCh3pzQtvEAAYASAAEgKxnfD_BwE) provides well-organized, high quality, and clear examples just because of the high popularity of elastic search tools in the market as compare to [Solr](https://lucene.apache.org/solr/guide/). 


# Conclusion
Both, Apache Solr and Elasticsearch Support similar features. Solr Fits better into Enterprise applications that already implemented big data tools like Hadoop. while Elasticsearch is more focused on scaling, data analytics, and processing time series data(like logs data) to obtain meaningful insights and patterns.
